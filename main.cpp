#include <iostream>
#include "BST.h"
using namespace std;

int main()
{
  Tree *root= NULL;
  Tree *Binary_tree;
  Binary_tree->insert_node(&root,80);
  Binary_tree->insert_node(&root,70);
  Binary_tree->insert_node(&root,11);
  Binary_tree->insert_node(&root,35);
  Binary_tree->insert_node(&root,96);
  Binary_tree->insert_node(&root,119);
  Binary_tree->insert_node(&root,23);
  Binary_tree->insert_node(&root,15);
  Binary_tree->insert_node(&root,76);
  Binary_tree->insert_node(&root,230);
  Binary_tree->insert_node(&root,78);
  Binary_tree->search_node(root,96);
  Binary_tree->search_node(root,55);
  Binary_tree->Inorder_Traversal(root);
  cout<<"\n\n";
  Binary_tree->Preorder_Traversal(root);
   cout<<"\n\n";
  Binary_tree->Postorder_Traversal(root);
  Binary_tree->delete_node(&root,96,NULL);
  cout<<"\n\n";
  Binary_tree->Inorder_Traversal(root);
  system("pause");
}