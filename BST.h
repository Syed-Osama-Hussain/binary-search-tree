#include <iostream>
using namespace std;

class Tree{
public:

Tree()
{
  this->value=0;
  this->left= NULL;
  this->right= NULL;
}

void insert_node(Tree **root,int val)
{
  Tree *curr= *root;
  if(*root==NULL)
  {
    Tree *ptr= NULL;
    ptr= (Tree*)malloc(sizeof(Tree));
    ptr->value= val;
    ptr->right= NULL;
    ptr->left= NULL;
    *root= ptr;
  }
  else
  {
    if(val>curr->value)
    {
      insert_node(&(curr->right),val);
    }
    else
    {
      if(val<curr->value)
      {
      insert_node(&(curr->left),val);
    }
    }
  }
}



void search_node(Tree *root,int search)
{
  Tree *curr= root;
  if(root==NULL)
  {
    cout<<"\n\nNo such node found in tree.\n\n";
    return;
  }
  if(root->value==search)
  {
    cout<<"\n\nNode is present in the tree.\n";
  }
  else
  {
    if(search>curr->value)
    {
      search_node(curr->right,search);
    }
    else
    {
      if(search<curr->value)
      {
      search_node(curr->left,search);
    }
    }
  }
}


void delete_node(Tree **start,int val,Tree **parent)
{
  Tree *curr= *start;
  if(curr==NULL)
  {
    cout<<"\nNo such node in tree.\n\n";
    return;
  }

  if(curr->value==val)
  {
    if(curr->left==NULL && curr->right==NULL)
    {
      free(curr);
      (*parent)->right= NULL;
      (*parent)->left= NULL;
      return;
    }
    else
    {
      if(curr->left==NULL || curr->right==NULL)
      {
        if(curr->left!=NULL)
        {
          if(parent!=NULL)
          {
          (*parent)->left = curr->left;
          free(curr);
          }
          else
          {
            *start= curr->left;
            free(curr);
          }
          return;
        }
        if(curr->right!=NULL)
        {
          if(parent!=NULL)
          {
          (*parent)->right= curr->right;
          free(curr);
          }
          else
          {
            *start= curr->right;
            free(curr);
          }
          return;
        }
      }
      
      else
      {        
        Tree *greatest= curr->left;
        while(greatest->right!=NULL)
        {
           greatest= greatest->right;
        }
        Tree *greatest_parent= search_parent(curr,greatest->value,NULL);

        if(parent!=NULL)
        {
        if((*parent)->left->value == val)
        {
          (*parent)->left= greatest;
        }
        else
        {
          (*parent)->right= greatest;
        }
        }
        else
        {
          *start= greatest;
        }

        
        greatest->right= curr->right;
        Tree *temp= greatest->left;
        greatest->left= curr->left;        
        greatest_parent->right= temp;  
        free(curr);
        return;
      }

    }
        return;
  }

   if(val>curr->value)
   {
     delete_node(&(curr->right),val,&curr);
   }

   if(val<curr->value)
   {
     delete_node(&(curr->left),val,&curr);
   }
}


Tree* search_parent(Tree *start,int val,Tree *parent)
{
Tree *curr= start;
  if(start==NULL)
  {
    cout<<"\n\nNo such node found in tree.\n\n";
    return NULL;
  }
  if(start->value==val)
  {
    //cout<<"\n\nNode is present in the tree.\n\n";
    return parent;
  }
  else
  {
    if(val>curr->value)
    {
      search_parent(curr->right,val,curr);
    }
    else
    {
      if(val<curr->value)
      {
      search_parent(curr->left,val,curr);
    }
    }
  }

} 


void Inorder_Traversal(Tree *root)
{
  if(root->left)
  {
    Inorder_Traversal(root->left);
  }
  cout<<root->value<<" ";
  if(root->right)
  {
    Inorder_Traversal(root->right);
  }
}


void Preorder_Traversal(Tree *root)
{
  cout<<root->value<<" ";
  if(root->left)
  {
    Preorder_Traversal(root->left);
  }
  if(root->right)
  {
    Preorder_Traversal(root->right);
  }
}

void Postorder_Traversal(Tree *root)
{
  if(root->left)
  {
    Postorder_Traversal(root->left);
  }
  if(root->right)
  {
    Postorder_Traversal(root->right);
  }
  cout<<root->value<<" ";
}


private:
int value;
Tree *right;
Tree *left;

};